-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2017 at 05:13 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `email`
--
CREATE DATABASE IF NOT EXISTS `email` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `email`;

-- --------------------------------------------------------

--
-- Table structure for table `email_contact`
--

CREATE TABLE IF NOT EXISTS `email_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `email_contact`
--

INSERT INTO `email_contact` (`id`, `name`, `email`) VALUES
(1, 'Saskii Amalia ', 'saskiiamalia@gmail.com'),
(2, 'Nabila Qonitah', 'nabilaql98@gmail.com'),
(3, 'Vergie Ericson', 'vergieet@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `email_schedule`
--

CREATE TABLE IF NOT EXISTS `email_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `send_at` datetime NOT NULL,
  `scheduled_at` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `template_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'waiting',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `email_schedule`
--

INSERT INTO `email_schedule` (`id`, `title`, `created_at`, `send_at`, `scheduled_at`, `created_by`, `template_id`, `status`) VALUES
(1, 'Tagihan Langganan First Media Bulan November', '2017-11-01 00:00:00', '2017-11-05 13:15:40', '2017-11-05 19:15:00', 'First Media', 1, 'sent'),
(2, 'Tagihan SPP Bulan November', '2017-11-02 00:00:00', '2017-11-05 13:12:30', '2017-11-05 19:10:00', 'SMK Telkom Malang', 1, 'sent'),
(3, 'Tagihan Air Bulan November', '2017-11-05 17:50:00', '2017-11-05 12:15:28', '2017-11-05 18:00:00', 'PDAM Kota Malang', 1, 'sent');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE IF NOT EXISTS `email_template` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_title` varchar(50) DEFAULT NULL,
  `email_content` text,
  `email_params` varchar(200) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`email_id`, `email_title`, `email_content`, `email_params`, `created_date`) VALUES
(1, 'Tagihan Bulan November', 'Sehubungan dengan ', NULL, '2017-11-04 10:54:32');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
